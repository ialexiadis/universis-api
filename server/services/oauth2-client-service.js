import unirest from 'unirest';
import url from 'url';
import _ from 'lodash';
import {ApplicationService} from '@themost/common/app';
import {HttpError} from '@themost/common/errors';

/**
 * @class
 */
class OAuth2ClientService extends ApplicationService {
    /**
     * @param {ExpressDataApplication} app
     */
    constructor(app) {
        super(app);
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * Gets user's profile by calling OAuth2 server profile endpoint
     * @param {ExpressDataContext} context
     * @param {string} token
     */
    getProfile(context, token) {
        return new Promise(function(resolve, reject) {
            return unirest.get(url.resolve(context.getApplication().getConfiguration().getSourceAt("settings/auth/server_uri"), "/me"))
                .header('Accept', 'application/json')
                .query({
                    "access_token":token,
                    "fields":"id,name"
                }).end(function (response) {
                    const responseBody =  response.body;
                    if (responseBody && responseBody.error) {
                        return reject(_.assign(new HttpError(responseBody.code || 500), responseBody));
                    }
                    return resolve(responseBody);
                });
        });
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * Gets the token info of the current context
     * @param {ExpressDataContext} context
     */
    getContextTokenInfo(context) {
        if (_.isNil(context.user)) {
            return Promise.reject(new Error('Context user may not be null'));
        }
        if (context.user.authenticationType !== 'Bearer') {
            return Promise.reject(new Error('Invalid context authentication type'));
        }
        if (_.isNil(context.user.authenticationToken)) {
            return Promise.reject(new Error('Context authentication data may not be null'));
        }
        return new Promise(function(resolve, reject) {
            //decrypt token
            const token = context.user && context.user.authenticationToken;
            return unirest.post(url.resolve(context.getApplication().getConfiguration().getSourceAt("settings/auth/server_uri"), "/tokeninfo"))
                .header('Accept', 'application/json')
                .query({
                    "access_token":token
                }).end(function (response) {
                    const responseBody =  response.body;
                    if (responseBody && responseBody.error) {
                        return reject(_.assign(new HttpError(responseBody.code || 500), responseBody));
                    }
                    return resolve(responseBody);
                });
        });
    }
    /**
     * Gets token info by calling OAuth2 server endpoint
     * @param {ExpressDataContext} context
     * @param {string} token
     */
    getTokenInfo(context, token) {
        return new Promise(function(resolve, reject) {
            return unirest.post(url.resolve(context.getApplication().getConfiguration().getSourceAt("settings/auth/server_uri"), "/tokeninfo"))
                .header('Accept', 'application/json')
                .query({
                    "access_token":token
                }).end(function (response) {
                    const responseBody =  response.body;
                    if (responseBody && responseBody.error) {
                        return reject(_.assign(new HttpError(responseBody.code || 500), responseBody));
                    }
                    return resolve(responseBody);
                });
        });
    }
}

module.exports.OAuth2ClientService = OAuth2ClientService;