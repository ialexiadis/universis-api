import { LangUtils, RandomUtils } from '@themost/common/utils';
import {DataModel} from '@themost/data/data-model';
import {DataModelView} from "@themost/data/data-model-view";
import _ from 'lodash';
import moment from 'moment';

const resolver = {
    today:function() {
        let callback;
        if (arguments.length === 0) {
            return moment().startOf('day').toDate();
        }
        else if (arguments.length === 1) {
            callback = arguments[0];
        }
        else {
            callback = arguments[1];
        }
        callback(null, moment().startOf('day').toDate());
    },
    lastMonth:function(callback)
    {
        callback(null,moment().subtract(1, 'month').toDate());
    },
    lastWeek:function(callback)
    {
        callback(null, moment().subtract(7, 'day').toDate());
    },
    lastDay:function(callback)
    {
        callback(null, moment().subtract(1, 'day').toDate());
    },
    lastHour:function(callback)
    {
        callback(null, moment().subtract(1, 'day').toDate());
    },
    me:function(callback) {
        /**
         * @type {DataModel|*}
         */
        const self = this, undefinedUser = 0;
        if (self.context) {
            const user =self.context.interactiveUser || self.context.user || { name:'anonymous' };
            if (_.isNil(user.id)) {
                //get user id
                const users = self.context.model('User');
                users.where('name').equal(user.name).select('id').flatten().silent().first(function(err, result) {
                    if (err) {
                        callback(err);
                    }
                    else {
                        if (_.isNil(result)) {
                            callback(null, undefinedUser);
                        }
                        else {
                            callback(null, result.id);
                        }
                    }
                });
            }
            else {
                callback(null, user.id);
            }
        }
        else {
            callback(null, undefinedUser);
        }
    },
    student:function(callback) {
        /**
         * @type {DataModel|*}
         */
        const self = this, undefinedStudent = 0;
        if (self.context) {
            const user = self.context.user || { name:'anonymous' };
            const users = self.context.model('User'), students = self.context.model('Student');
            let username=user.name;
            if (self.context.interactiveUser && self.context.interactiveUser.name)
                username=self.context.interactiveUser.name;
            students.join('User').where(users.fieldOf('name')).equal(username).select([students.fieldOf(students.primaryKey)]).flatten().silent().first(function(err, result) {
                if (err) {
                    callback(err);
                }
                else {
                    if (_.isNil(result)) {
                        callback(null, undefinedStudent);
                    }
                    else {
                        //get student user
                        callback(null, result[students.primaryKey]);
                    }
                }
            });
        }
        else {
            callback(null, undefinedStudent);
        }
    },
    department:function(callback) {
        /**
         * @type {DataModel|*}
         */
        const self = this;
        if (self.context) {
            const user = self.context.user || { name:'anonymous' };
            const users = self.context.model('User');
            let username=user.name;
            if (self.context.interactiveUser && self.context.interactiveUser.name)
                username=self.context.interactiveUser.name;
            users.where('name').equal(username).select(['id','departments']).expand('departments').silent().first(function(err, result) {
                if (err) {
                    callback(err);
                }
                else {
                    if (_.isNil(result)) {
                        callback(null, 0);
                    }
                    else if (_.isNil(result['departments'][0])) {
                        callback(null, 0);
                    }
                    else {
                        //get user department
                        callback(null, result['departments'][0].id);
                    }
                }
            });
        }
        else {
            callback(null, 0);
        }
    },
    currentYear:function(callback) {
        /**
         * @type {DataModel|*}
         */
        const self = this;
        if (self.context) {
            const user = self.context.user || { name:'anonymous' };
            const users = self.context.model('User');
            let username=user.name;
            if (self.context.interactiveUser && self.context.interactiveUser.name)
                username=self.context.interactiveUser.name;
            users.where('name').equal(username).select(['id','departments']).expand('departments').silent().first(function(err, result) {
                if (err) {
                    callback(err);
                }
                else {
                    if (_.isNil(result)) {
                        callback(null, 0);
                    }
                    else if (_.isNil(result['departments'][0])) {
                        callback(null, 0);
                    }
                    else {
                        callback(null, LangUtils.parseInt(result['departments'][0].currentYear));
                    }
                }
            });
        }
        else {
            callback(null, 0);
        }
    },
    currentPeriod:function(callback) {
        /**
         * @type {DataModel|*}
         */
        const self = this;
        if (self.context) {
            const user = self.context.user || { name:'anonymous' };
            const users = self.context.model('User');
            let username=user.name;
            if (self.context.interactiveUser && self.context.interactiveUser.name)
                username=self.context.interactiveUser.name;
            users.where('name').equal(username).select(['id','departments']).expand('departments').silent().first(function(err, result) {
                if (err) {
                    callback(err);
                }
                else {
                    if (_.isNil(result)) {
                        callback(null, 0);
                    }
                    else if (_.isNil(result['departments'][0])) {
                        callback(null, 0);
                    }
                    else {
                        callback(null, LangUtils.parseInt(result['departments'][0].currentPeriod));
                    }
                }
            });
        }
        else {
            callback(null, 0);
        }
    }
    ,
    instructor:function(callback) {
        /**
         * @type {DataModel|*}
         */
        const self = this, undefinedInstructor = -(RandomUtils.randomInt(1,1000000));
        if (self.context) {
            const user = self.context.user || { name:'anonymous' };
            const instructors = self.context.model('Instructor');
            let username=user.name;
            if (self.context.interactiveUser && self.context.interactiveUser.name)
                username=self.context.interactiveUser.name;
            instructors.where("user/name").equal(username).select(['id']).flatten().silent().first(function(err, result) {
                if (err) {
                    callback(err);
                }
                else {
                    if (_.isNil(result)) {
                        callback(null, undefinedInstructor);
                    }
                    else {
                        //get instructor user
                        callback(null, result['id']||undefinedInstructor);
                    }
                }
            });
        }
        else {
            callback(null, undefinedInstructor);
        }
    },
    program:function(callback) {
        /**
         * @type {DataModel|*}
         */
        const self = this;
        if (self.context) {
            const user = self.context.user || { name:'anonymous' };
            const users = self.context.model('User'), students = self.context.model('Student');
            let username=user.name;
            if (self.context.interactiveUser && self.context.interactiveUser.name)
                username=self.context.interactiveUser.name;
            students.join('User').where(users.fieldOf('name')).equal(username).select([students.fieldOf('program')]).flatten().silent().first(function(err, result) {
                if (err) {
                    callback(err);
                }
                else {
                    if (_.isNil(result)) {
                        callback(null, 0);
                    }
                    else {
                        //get student user
                        callback(null, result['program']);
                    }
                }
            });
        }
        else {
            callback(null, 0);
        }
    }
};

/**
 * @name DataModel~resolveMethod
 * @param {string} name
 * @param {*} args
 * @param {Function} callback
 *
 */

_.assign(DataModel.prototype, {
    /**
     * @param {string} name
     * @param {*} args
     * @param {Function} callback
     */
    resolveMethod: function(name, args, callback) {
        if (typeof resolver[name] === 'function') {
            const args1 = args || [];
            args1.push(callback);
            resolver[name].apply(this, args1);
        }
        else {
            callback();
        }
    }
});

_.assign(DataModelView.prototype, {
    /**
     * @method
     * @name DataModelView~toLocaleArray
     * @param {Array<*>} arr
     * @returns {Array<*>}
     */
    toLocaleArray: function(arr) {
        const self = this;
        if (Array.isArray(arr)) {
            // map attributes
            let attributes = this.attributes.map( attribute=> {
                // get title
               let title = attribute.title || attribute.getName();
               return {
                   name: attribute.getName(),
                   title: self.model.context.__(title)
               }
            });
            return arr.map( item => {
                return attributes.reduce( (obj, attribute) => {
                    // set value
                    obj[attribute.title] = item[attribute.name];
                    return obj;
                }, { });
            });
        }
        throw new TypeError('Expected array of objects');
    },
    /**
     * @method
     * @name DataModelView~fromLocaleArray
     * @param {Array<*>} arr
     * @returns {Array<*>}
     */
    fromLocaleArray: function(arr) {
        const self = this;
        if (Array.isArray(arr)) {
            // map attributes
            let attributes = this.attributes.map( attribute=> {
                // get title
                let title = attribute.title || attribute.getName();
                return {
                    name: attribute.getName(),
                    title: self.model.context.__(title)
                }
            });
            return arr.map( item => {
                return attributes.reduce( (obj, attribute) => {
                    // set value
                    obj[attribute.name] = item[attribute.title];
                    return obj;
                }, { });
            });
        }
        throw new TypeError('Expected array of objects');
    }

});