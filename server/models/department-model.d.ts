import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import AcademicYear = require('./academic-year-model');
import AcademicPeriod = require('./academic-period-model');
import StudyLevel = require('./study-level-model');

/**
 * @class
 */
declare class Department extends DataObject {

     
     /**
      * @description Μοναδικός κωδικός τμήματος
      */
     public id: string; 
     
     /**
      * @description Το ίδρυμα που ανήκει το τμήμα.
      */
     public organization: string; 
     
     /**
      * @description Η πλήρης ονομασία τμήματος
      */
     public name?: string; 
     
     /**
      * @description Η συντομογραφία του ονόματος του τμήματος
      */
     public abbreviation?: string; 
     
     /**
      * @description Η όλη που εδρεύει το τμήμα.
      */
     public city?: string; 
     
     /**
      * @description Η ταχυδρομική διεύθυνση του τμήματος
      */
     public address?: string; 
     
     /**
      * @description Ο ταχυδρομικός κώδικας
      */
     public postalCode?: string; 
     
     /**
      * @description Χώρα
      */
     public country?: string; 
     
     /**
      * @description Εναλλακτικός κωδικός που μπορεί να συνδέσει το τμήμα με τον αντίστοιχο κωδικό κάποιου άλλου εξωτερικού συστήματος πχ υπουργείο κλπ
      */
     public alternativeCode?: string; 
     
     /**
      * @description Το τρέχον ακαδημαϊκό έτος του τμήματος
      */
     public currentYear?: AcademicYear|any; 
     
     /**
      * @description Τρέχουσα ακαδημαϊκή περίοδος
      */
     public currentPeriod?: AcademicPeriod|any; 
     
     /**
      * @description Η ονομασία της σχολής εφόσον το τμήμα ανήκει σε σχολή.
      */
     public facultyName?: string; 
     
     /**
      * @description Τηλέφωνο επικοινωνίας 1
      */
     public phone1?: string; 
     
     /**
      * @description Τηλέφωνο επικοινωνίας 2
      */
     public phone2?: string; 
     
     /**
      * @description Το επίπεδο σπουδών των προγραμμάτων σπουδών που προσφέρονται από το τμήμα. (πχ προπτυχιακό, μεταπτυχιακό, διδακτορικό).
      */
     public studyLevel?: StudyLevel|any; 
     
     /**
      * @description Όνομα υπεύθυνου επικοινωνίας 1
      */
     public contactPerson1?: string; 
     
     /**
      * @description Όνομα υπεύθυνου επικοινωνίας 2
      */
     public contactPerson2?: string; 
     
     /**
      * @description Η διεύθυνση ηλεκτρονικού ταχυδρομείου του τμήματος.
      */
     public email?: string; 
     
     /**
      * @description Ο τυπικός αριθμός εξαμήνων που διαρκεί ένα πρόγραμμα σπουδών του τμήματος
      */
     public totalSemesters?: string; 
     
     /**
      * @description Η ηλεκτρονική διεύθυνση
      */
     public url?: string; 
     
     /**
      * @description Αν είναι τοπικό τμήμα ή δεν ανήκει στο Ίδρυμα.
      */
     public localDepartment?: boolean; 
     
     /**
      * @description Ημερομηνία τελευταίας τροποποίησης
      */
     public dateModified: Date; 

}

export = Department;