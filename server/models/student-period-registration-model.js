import util from 'util';
import {TraceUtils} from '@themost/common/utils';
import { QueryEntity } from '@themost/query/query';
import {DataObject} from "@themost/data/data-object";
import {ValidationResult} from "../errors";
import _ from 'lodash';
import {LangUtils} from '@themost/common/utils';
import StudentRegisterActionModel from "./student-register-action-model";
import {EdmMapping} from "@themost/data/odata";

@EdmMapping.entityType('StudentPeriodRegistration')
/**
 * @class
 * @augments DataObject
 */
class StudentPeriodRegistration extends DataObject {
    constructor() {
        super();
        //register custom selectors
        const self = this;
        self.selector('current', function(done) {
            const context = self.context;
            self.studentOf(function(err, result) {
                if (err) { return done(err); }
                if (_.isNil(result)) { return done(new Error('Student is null or undefined')); }
                /**
                 *
                 * @type {Student|*}
                 */
                const student = context.model('Student').convert(result);
                student.departmentOf(function(err, result) {
                    if (err) { return done(err); }
                    if (_.isNil(result)) { return done(new Error('Student department is null or undefined')); }
                    context.model('Department').where('id').equal(result).select( "id", "name", "abbreviation", "currentYear", "currentPeriod").flatten().silent().first(function(err, department) {
                        if (err) { return done(err); }
                        if (_.isNil(department)) { return done(new Error('Student department is null or undefined')); }
                        self.attrOf('registrationYear', function(err, registrationYear) {
                            if (_.isNil(registrationYear)) { return done(new Error('Registration year is null or undefined')); }
                            if (err) { return done(err); }
                            registrationYear = context.model('AcademicYear').convert(registrationYear).getId();
                            self.attrOf('registrationPeriod', function(err, registrationPeriod) {
                                if (err) { return done(err); }
                                if (_.isNil(registrationPeriod)) { return done(new Error('Registration period is null or undefined')); }
                                registrationPeriod = context.model('AcademicPeriod').convert(registrationPeriod).getId();
                                done(null, (registrationYear === department.currentYear) && (registrationPeriod === department.currentPeriod));
                            });
                        });
                    });
                });
            });
        });

    }

    validateState(done) {
        const self = this, context = self.context;
        self.studentOf(function(err, result) {
            if (err) { return done(err); }
            if (_.isNil(result)) { return done(new Error('Student is null or undefined')); }
            /**
             *
             * @type {Student|*}
             */
            const student = context.model('Student').convert(result);
            student.is(':active').then(function(active) {
                if (!active) {
                    return done(new Error('Student is not active.'));
                }
                //validate student registration state
                self.statusOf(function(err, result) {
                    if (err) { return done(err); }
                    if (result === 2) {
                        return done(new Error('Student registration cannot be modified due to its state.'));
                    }
                    //validate department registration period
                    student.departmentOf(function(err, result) {
                        if (err) { return done(err); }
                        if (_.isNil(result)) { return done(new Error('Student department is null or undefined')); }
                        const currentDate=new Date();
                        currentDate.setHours(0,0,0,0);
                        context.model('LocalDepartment').where('id').equal(result)
                            .and('date(registrationPeriodStart)').lowerOrEqual(currentDate)
                            .and('date(registrationPeriodEnd)').greaterOrEqual(currentDate)
                            .select( "id", "name", "registrationPeriodStart","registrationPeriodEnd").flatten().silent().first(function(err, department) {
                                if (err) { return done(err); }
                                //check also if there is a referrer and ignore this error
                                if (_.isNil(department) && (!(self.action instanceof StudentRegisterActionModel))) {
                                    return done(new Error(context.__('Invalid student registration period.')));
                                }
                                else {
                                    //check also if database is online
                                    context.model('Workspace').where('databaseStatus').equal('online').flatten().silent().first(function (err, workspace) {
                                        if (err) {
                                            return done(err);
                                        }
                                        if (_.isNil(workspace)) {
                                            return done(new Error('The system is updating. Please try again later.'));
                                        }
                                        else {
                                            done();
                                        }
                                    });
                                }
                            });
                    });
                });
            }).catch(function(err) {
                done(err);
            });

        });
    }

    studentOf(callback) {
        const self = this;
        self.attrOf('student', function(err, result) {
            if (err) { return callback(err); }
            if (_.isNil(result)) {
                //get current student
                self.getModel().resolveMethod('student',[], function(err, result) {
                    if (err) { return callback(err); }
                    self['student'] = result;
                    callback(null, result);
                });
            }
            else {
                callback(null, result);
            }
        });
    }

    statusOf(callback) {
        const self = this, context = self.context;
        self.studentOf(function(err, result) {
            if (err) {
                return callback(err);
            }
            if (_.isNil(result)) {
                return callback(new Error('Student is null or undefined'));
            }
            self.attrOf('registrationYear', function (err, registrationYear) {
                if (err) {
                    return callback(err);
                }
                self.attrOf('registrationPeriod', function (err, registrationPeriod) {
                    if (err) {
                        return callback(err);
                    }
                    context.model('StudentPeriodRegistration').where('student').equal(result).and('registrationYear').equal(registrationYear).and('registrationPeriod').equal(registrationPeriod).select( "id", "status").flatten().silent().first(function (err, registration) {
                        if (err) {
                            return callback(err);
                        }
                        if (!_.isNil(registration)) {
                            self.id = registration["id"];
                            return callback(null, registration["status"]);
                        }
                        else
                            return(callback(null, 3));
                    });
                });
            });
        });
    }

    inferStudentSemester(callback) {
        const self = this;
        try {
            const students = self.context.model('Student'), studentObj = students.convert(self.student);
            if (!_.isNil(self.id)) {
                return callback();
            }
            else {
               const registrationPeriod = self.context.model('AcademicPeriod').convert(self.registrationPeriod).getId();
                studentObj.inferSemester(self.registrationYear,registrationPeriod,function(err, semester) {
                    students.save({ id:studentObj.id, semester:semester }, function(err) {
                        if (err) { return callback(err); }
                        self.semester = semester;
                        callback();
                    })
                });
            }
        }
        catch (e) {
            callback(e);
        }
    }

    save(context, callback) {
        try {
            const self = this, saveFunc = super.save;
            //ensure classes list
            self.classes = self.classes || [];
            //get original classes
            self.validateState(function(err) {
               if (err) {
                   self.validationResult = new ValidationResult(false,err.code || 'EFAIL',self.context.__('The registration is not valid.'), err.message);
                   return callback();
               }
                self.is(':live').then(function(live) {
                    //set registration status to 3 (pending registration)
                    self.status = 3;
                    self.studentOf(function(err, student) {
                        //query student classes (which are already registered if any)
                        const studentCourseClasses = self.context.model('StudentCourseClass'), view = studentCourseClasses.dataviews('RegistrationRequest'), availableClasses = self.context.model('StudentAvailableClass');
                        studentCourseClasses.where('registration').equal(LangUtils.parseInt(self['id'])).and('student').equal(student).flatten().silent().all(function(err, registeredClasses) {
                            if (err) { return callback(err); }
                            //get requested classes
                            const requestClasses = view.cast(self.classes);

                            //get available classes (based on requested classes)
                            const studentAvailableClasses = context.model('StudentAvailableClass');

                            let values = requestClasses.map(function(x) {
                                if (x.courseClass && x.courseClass.id) {
                                    return x.courseClass.id;
                                }
                                return x.courseClass;
                            });
                            if (values.length===0) {
                                values.push(null);
                            }
                            let q = studentAvailableClasses.asQueryable().where('courseClass').in(values).silent().orderBy('registered').thenByDescending('specialty');
                            let registrationYear = self.context.model('AcademicYear').convert(self.registrationYear).getId();
                            let registrationPeriod = self.context.model('AcademicPeriod').convert(self.registrationPeriod).getId();
                            //change entity definition
                            let entityExpr = util.format('ufnStudentAvailableClasses(%s,%s,%s)',registrationYear, registrationPeriod, student);
                            let entity = new QueryEntity(entityExpr).as('StudentAvailableClasses');
                            q.query.from(entity);
                            //execute query and get available classes
                            q.all(function(err, availableClasses) {
                                if (err) {
                                    return callback(err);
                                }
                                //validate requested classes
                                if ((self.classes.length>0) && (self.classes.length !== availableClasses.length)) {
                                    //enumerate classes and apply validation results for unavailable class
                                    let availableClass;

                                    self.classes.forEach(function(x) {
                                        availableClass = availableClasses.find(function(y) {
                                            if (_.isString(x.courseClass) && _.isString(y.courseClass)) {
                                                return x.courseClass === y.courseClass;
                                            }
                                        });
                                        if (typeof availableClass === 'undefined') {
                                            x.validationResult = new ValidationResult(false,'EAVAIL','The course specified is unavailable for the selected academic year and period.');
                                        }
                                    });
                                    self.validationResult = new ValidationResult(false,'EFAIL','The registration contains unavailable courses.');
                                    callback();
                                }
                                else {
                                    //delete unused properties
                                    //delete availableClasses.columns;
                                    //extend available classes with the request data
                                    availableClasses.forEach(function(x) {
                                        //delete unused properties
                                        delete x.columns;
                                        const requestClass = requestClasses.find(function(y) { return y.courseClass === x.courseClass; });
                                        if (requestClass) {
                                            Object.assign(x, requestClass);
                                        }
                                        //remove id for student class compatibility issues
                                        delete x.id;

                                    });
                                    //push available class to registration classes collection
                                    self.classes = availableClasses;
                                    context.db.executeInTransaction(function(cb) {
                                        //remove classes which are already in registration
                                        const studentCourseClasses = context.model('StudentCourseClass'), registrationPeriod = context.model('AcademicPeriod').convert(self.registrationPeriod).getId();
                                        values = self.classes.map(function(x) { return x.courseClass; });
                                        studentCourseClasses.where('student').equal(student)
                                            .and('registration/registrationYear').equal(self.registrationYear)
                                            .and('registration/registrationPeriod').equal(registrationPeriod)
                                            .select('id', 'registration', 'student', 'courseClass','course/displayCode as displayCode').flatten().silent().all(function(err, result) {
                                                if (err) { return cb(err); }

                                                self.classes.forEach(function(x) {x.$state=1 });
                                                result.forEach(function(x) {
                                                    const studentCourseClass = self.classes.find(function(y) { return y.courseClass.toString() === x.courseClass.toString() });
                                                    if (studentCourseClass) {
                                                        //set modified state
                                                        studentCourseClass.$state = 2;
                                                    }
                                                    else {
                                                        //set deleted state
                                                        x.$state = 4;
                                                        self.classes.push(x);
                                                    }
                                                });

                                                self.inferStudentSemester(function(err) {
                                                    if (err) { return cb(err); }
                                                    //save registration (and classes)
                                                    saveFunc.bind(self)(context, function(err) {
                                                        if (err) { return cb(err); }
                                                        self.createDocument(function(err){
                                                            cb(err);
                                                        });
                                                    });
                                                });
                                            });
                                    }, function(err) {
                                        if (err) {
                                            if (err instanceof ValidationResult) {
                                                self.validationResult = err;
                                            }
                                            else {
                                                TraceUtils.error(err);
                                                self.validationResult = new ValidationResult(false,err.code || 'EFAIL',self.context.__('The registration is not valid.'), err.message);
                                            }
                                        }
                                        else {
                                            const failed = self.classes.find(function (x) {
                                                if (typeof x.validationResult === 'undefined')
                                                    return false;
                                                return !x.validationResult.success;
                                            });
                                            if (typeof self.validationResult === 'undefined') {
                                                if (typeof failed !== 'undefined') {
                                                    self.validationResult = new ValidationResult(true, 'PSUCC', self.context.__('The registration was completed successfully with errors.'));
                                                }
                                                else {
                                                    self.validationResult = new ValidationResult(true, 'SUCC', self.context.__('The registration was completed successfully.'));
                                                }

                                            }

                                        }
                                        callback();
                                    });
                                }
                            });
                        });
                    });
                }, function(err) {
                    callback(err);
                });

            });

        }
        catch(er) {
            callback(er);
        }

    }

    createDocument(callback) {
        try {
            const self = this, context = self.context;
            self.studentOf(function (err, student) {
                //add registration document
                const inserted = self.classes.filter(function (x) {
                    if (typeof x.validationResult === 'undefined')
                        return false;
                    return x.validationResult.success && x.validationResult.code === 'SUCC';
                });
                const deleted = self.classes.filter(function (x) {
                    if (typeof x.validationResult === 'undefined')
                        return false;
                    return x.validationResult.success && x.validationResult.code === 'DEL';
                });
                if (deleted.length === 0 && inserted.length === 0 && self.classes.length!==0) {
                    return callback();
                }
                const registrationYear = context.model('AcademicYear').convert(self.registrationYear).getId();
                const registrationPeriod = context.model('AcademicPeriod').convert(self.registrationPeriod).getId();
                const query = util.format("sp_createStudentRegistrationDocument %s,%s,%s", registrationYear, registrationPeriod, student);
                context.db.execute(query, null, function (err, result) {
                    if (err) {
                        TraceUtils.error(err);
                    }
                    else {
                        try {
                            let insClasses = "", delClasses = "";
                            if (inserted.length > 0) {
                                insClasses = util.format('%s:%s', context.__('Inserted courses'), inserted.map(function (x) {
                                    return util.format('%s', x.displayCode)
                                }).join(', '));
                            }
                            if (deleted.length > 0) {
                                delClasses = util.format('%s:%s', context.__('Deleted courses'), deleted.map(function (x) {
                                    return util.format('%s', x.displayCode)
                                }).join(', '));
                            }
                            let eventTitle = "";
                            if (self.classes.length === 0) {
                                eventTitle = util.format("Η δήλωση του φοιτητή με περιγραφή [%s] Αποστολή δήλωσης [%s][%s] ] ακαδ.έτους %s-%s αποθηκεύθηκε. Η κατάστασή της είναι <%s>"
                                    , self.id, result[0]['name'], result[0]['studentIdentifier'], result[0]['registrationYear'], result[0]['registrationYear'] + 1, result[0]['docStatusReason']);
                            }
                            else
                                eventTitle = util.format('Τροποποιήσεις δήλωσης [%s] %s  %s-%s %s,%s"'
                                    , result[0]['studentIdentifier'], result[0]['name'], result[0]['registrationYear'], result[0]['registrationYear'] + 1, insClasses, delClasses);

                            context.unattended(function (cb) {
                                context.model('EventLog').save({
                                    title: eventTitle,
                                    eventType: 3,
                                    username: context.interactiveUser.name
                                }, function (err) {
                                    cb(err);
                                });
                            }, function (err) {
                                if (err) {
                                    TraceUtils.error(err);
                                }
                                callback();
                            });
                        }
                        catch (e) {
                            return callback(e);
                        }
                    }
                });
            });
        }
        catch(e) {
            callback(e);
        }
    }
}

module.exports = StudentPeriodRegistration;
