import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import StudentPeriodRegistration = require('./student-period-registration-model');
import CourseClass = require('./course-class-model');
import Student = require('./student-model');
import RegistrationType = require('./registration-type-model');
import ExamPeriod = require('./exam-period-model');
import Semester = require('./semester-model');
import CourseType = require('./course-type-model');
import Course = require('./course-model');

/**
 * @class
 */
declare class StudentCourseClass extends DataObject {

     
     /**
      * @description Ο μοναδικός κωδικός της εγγραφής.
      */
     public id: string; 
     
     /**
      * @description Η εγγραφή του φοιτητή σε συγκεκριμένο έτος και περίοδο.
      */
     public registration: StudentPeriodRegistration|any; 
     
     /**
      * @description Η τάξη του μαθήματος
      */
     public courseClass: CourseClass|any; 
     
     /**
      * @description Φοιτητής
      */
     public student: Student|any; 
     
     /**
      * @description Ο τελικός βαθμός της δήλωσης του μαθήματος
      */
     public finalGrade?: number; 
     
     /**
      * @description Τύπος δήλωσης
      */
     public registrationType?: RegistrationType|any; 
     
     /**
      * @description Αριθμός απουσιών του φοιτητή στο μάθημα για το έτος και την περίοδο.
      */
     public absences?: number; 
     
     /**
      * @description Η τελευταία εξεταστική περίοδος που βαθμολογήθηκε ο φοιτητής για την τάξη.
      */
     public examPeriod?: ExamPeriod|any; 
     
     /**
      * @description Αν η δήλωση της τάξης έγινε με αυτόματη διαδικασία.
      */
     public autoRegistered?: number; 
     
     /**
      * @description Ο βαθμός της τελευταίας εξέτασης της τάξης.
      */
     public examGrade?: number; 
     
     /**
      * @description Τμήμα τάξης
      */
     public section?: number; 
     
     /**
      * @description Η κατάσταση της δήλωσης της τάξης.
      */
     public status?: number; 
     
     /**
      * @description Διδακτικές μονάδες
      */
     public units?: number; 
     
     /**
      * @description Ο συντελεστής πτυχίου
      */
     public coefficient?: number; 
     
     /**
      * @description Ώρες διδασκαλίας
      */
     public hours?: string; 
     
     /**
      * @description Το εξάμηνο χρέωσης του μαθήματος στο φοιτητή.
      */
     public semester: Semester|any; 
     
     /**
      * @description Ο τύπος του μαθήματος στο φοιτητή.
      */
     public courseType: CourseType|any; 
     
     /**
      * @description Μονάδες ECTS
      */
     public ects?: number; 
     
     /**
      * @description percentileRank
      */
     public percentileRank?: number; 
     
     /**
      * @description Ημερομηνία τροποποίησης
      */
     public dateModified: Date; 
     
     /**
      * @description Μάθημα
      */
     public course: Course|any; 
     
     /**
      * @description Το μάθημα έχει περαστεί
      */
     public isPassed?: number; 
     
     /**
      * @description Βαθμός (στην κλίμακα)
      */
     public formattedGrade?: string; 
     
     /**
      * @description Τμήμα τάξης (ονομασία)
      */
     public sectionName?: string; 

}

export = StudentCourseClass;