import {DataObject} from "@themost/data/data-object";
import {EdmMapping} from "@themost/data/odata";

@EdmMapping.entityType('User')
/**
 * @class
 * @augments {DataObject}
 */
class User extends DataObject {
    constructor() {
        super();
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * Gets the student associated with this user
     * @returns {Promise<Student>}
     */
    getStudent() {
        const context = this.getModel().context;
        return context.model("Student").where("userId/name").equal(this.name).silent().getTypedItem();
    }

    @EdmMapping.func("Me", "User")
    /**
     * Returns an entity which represents the current user
     */
    static getMe(context) {
        const model = context.model('User');
        return new Promise(((resolve, reject) => {
            model.filter("id eq me()", (err, q) => {
                if (err) {
                    return reject(err);
                }
                return resolve(q);
            });
        }));
    }
}

module.exports = User;