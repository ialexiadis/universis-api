import StudentMapper from './student-mapper';
import {DataObject} from "@themost/data/data-object";
import {ValidationResult} from "../errors";
import {TraceUtils} from '@themost/common/utils';
import _ from 'lodash';
import {EdmMapping} from "@themost/data/odata";

@EdmMapping.entityType('StudentRegisterAction')
/**
 * @class
 * @augments StudentMapper
 */
class StudentRegisterAction extends DataObject {
    constructor() {
        super();
    }

    save(context, callback) {
        const self = this, saveFunc = super.prototype.save;
        try {
            self.validateState(function (err) {
                if (err) {
                    self.validationResult = new ValidationResult(false, err.code || 'EFAIL', context.__('Cannot register to current semester.'), err.message);
                    return callback();
                }
                saveFunc.bind(self)(context, function (err) {
                    if (err) {
                        if (err instanceof ValidationResult) {
                            self.validationResult = err;
                        }
                        else {
                            TraceUtils.error(err);
                            self.validationResult = new ValidationResult(false, err.code || 'FAIL', context.__('Cannot register to current semester.'), err.message);
                        }
                    }
                    else {
                        self.validationResult = new ValidationResult(true, 'SUCC', context.__('Student successfully registered.'));
                    }
                    callback();
                });
            });
        }
        catch (er) {
            callback(er);
        }
    }

    validateState(callback) {
        const self = this, context = self.context;
        //check registration dates
        const currentDate = new Date();
        currentDate.setHours(0, 0, 0, 0);
        context.model('LocalSettings').where('name').equal('registrationPeriod')
            .and('date(date1)').lowerOrEqual(currentDate)
            .and('date(date2)').greaterOrEqual(currentDate)
            .select( "id", "name").flatten().silent().first(function (err, result) {
                if (err) {
                    return callback(err);
                }
                if (_.isNil(result)) {
                    return callback(new Error(context.__('Invalid student registration period.')));
                }
                else {
                    //check also if database is online
                    context.model('Workspace').where('databaseStatus').equal('online').flatten().silent().first(function (err, workspace) {
                        if (err) {
                            return callback(err);
                        }
                        if (_.isNil(workspace)) {
                            return callback(new Error('The system is updating. Please try again later.'));
                        }
                        else {
                            callback();
                        }
                    });
                }
            });
    }
}


StudentRegisterAction.prototype.studentOf = StudentMapper.prototype.studentOf;

module.exports = StudentRegisterAction;
