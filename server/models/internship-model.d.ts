import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import Department = require('./department-model');
import Student = require('./student-model');
import AcademicYear = require('./academic-year-model');
import AcademicPeriod = require('./academic-period-model');
import InternshipStatus = require('./internship-status-model');

/**
 * @class
 */
declare class Internship extends DataObject {

     
     /**
      * @description Ο μοναδικός κωδικός της πρακτικής άσκησης
      */
     public id: number; 
     
     /**
      * @description Η ονομασία της πρακτικής άσκησης
      */
     public name?: string; 
     
     /**
      * @description Το τμήμα που προσφέρει την πρακτική άσκηση.
      */
     public department: Department|any; 
     
     /**
      * @description Φοιτητής
      */
     public student?: Student|any; 
     
     /**
      * @description Η εταιρεια-οργανισμός που προσφέρει την πρακτική άσκηση
      */
     public company?: number; 
     
     /**
      * @description Ο υπεύθυνος επικοινωνίας της εταιρείας-οργανισμού.
      */
     public companyContact?: string; 
     
     /**
      * @description Το τηλέφωνο επικοινωνίας του υπεύθυνου της εταιρείας-οργανισμού.
      */
     public companyContactPhone?: string; 
     
     /**
      * @description Το ακαδημαϊκό έτος διεξαγωγής της πρακτικής άσκησης.
      */
     public internshipYear: AcademicYear|any; 
     
     /**
      * @description Η ακαδημαϊκή περίοδος διεξαγωγής της πρακτικής άσκησης.
      */
     public internshipPeriod?: AcademicPeriod|any; 
     
     /**
      * @description Η ημερομηνία εγγραφής στην πρακτική άσκηση
      */
     public internshipDate?: Date; 
     
     /**
      * @description Η ημερομηνία έναρξης της πρακτικής άσκησης
      */
     public startDate?: Date; 
     
     /**
      * @description Η ημερομηνία λήξης της πρακτικής άσκησης.
      */
     public endDate?: Date; 
     
     /**
      * @description Η κατάσταση της πρακτικής άσκησης.
      */
     public status: InternshipStatus|any; 
     
     /**
      * @description Η ημερομηνία ολοκλήρωσης της πρακτικής άσκησης.
      */
     public dateCompleted?: Date; 
     
     /**
      * @description Ημερομηνία τροποποίησης
      */
     public dateModified: Date; 

}

export = Internship;