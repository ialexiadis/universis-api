import util from 'util';
import _ from 'lodash';
import async from 'async';
import StudentMapper from './student-mapper';
import {ValidationResult} from "../errors";
import {DataObject} from "@themost/data/data-object";
import {TraceUtils} from "@themost/common/utils";
import {EdmMapping} from "@themost/data/odata";

@EdmMapping.entityType('StudentSpecialty')
/**
 * @class
 * @property {*} specialty
 * @property {*} student
 * @augments StudentMapper
 */
class StudentSpecialty extends DataObject {
    constructor(obj) {
        super('StudentSpecialty', obj);
    }

    validate(callback) {
        const self = this, context = self.context;
        try {
            self.studentOf(function (err, result) {
                if (err) {
                    return callback(err);
                }
                if (_.isNil(result)) {
                    return callback(new Error(context.__('Student is null or undefined')));
                }
                /**
                 *
                 * @type {Student|*}
                 */
                const student = context.model('Student').convert(result);
                student.programOf(function (err, program) {
                    const rules = context.model('Rule');
                    let validationResults;
                    const additionalTypes = ['SpecialtyRule'];
                    const q = rules.where('target').equal(program).and('additionalType').in(additionalTypes).and('programSpecialty').equal(self.specialty.specialty).prepare(true);

                    q.silent().all(function (err, rulesData) {
                        if (err) {
                            return callback(err);
                        }
                        if (rulesData.length === 0) {
                            return callback(null, [new ValidationResult(true, 'SUCC', context.__('Student semester selection does not have any rule.'))]);
                        }
                        async.eachSeries(rulesData, function (item, cb) {
                            const ruleModel = context.model(item.refersTo + 'Rule');
                            if (_.isNil(ruleModel)) {
                                return callback(new Error(context.__('Student semester selection rule type cannot be found.')));
                            }
                            const rule = ruleModel.convert(item);
                            rule.validate(self, function (err, result) {
                                if (err) {
                                    return callback(err);
                                }
                                /**
                                 * @type {ValidationResult[]}
                                 */
                                validationResults = validationResults || [];
                                validationResults.push(result);
                                cb();
                            });
                        }, function (err) {
                            if (err) {
                                return callback(err);
                            }

                            const fnValidateExpression = function (x) {
                                try {
                                    let expr = x['ruleExpression'];
                                    if (_.isEmpty(expr)) {
                                        return false;
                                    }
                                    expr = expr.replace(/\[%(\d+)]/g, function () {
                                        if (arguments.length === 0) return;
                                        const id = parseInt(arguments[1]);
                                        const v = validationResults.find(function (y) {
                                            return y.id === id;
                                        });
                                        if (v) {
                                            return v.success.toString();
                                        }
                                        return 'false';
                                    });
                                    expr = expr.replace(/\bAND\b/g, ' && ').replace(/\bOR\b/g, ' || ').replace(/\bNOT\b/g, ' !');
                                    return eval(expr);
                                }
                                catch (e) {
                                    return e;
                                }
                            };

                            const fnTitleExpression = function (x) {
                                try {
                                    let expr = x['ruleExpression'];
                                    if (_.isEmpty(expr)) {
                                        return false;
                                    }
                                    expr = expr.replace(/\[%(\d+)]/g, function () {
                                        if (arguments.length === 0) return;
                                        const id = parseInt(arguments[1]);
                                        const v = validationResults.find(function (y) {
                                            return y.id === id;
                                        });
                                        if (v) {
                                            return '(' + v.message.toString() + ')';
                                        }
                                        return 'Uknown Rule';
                                    });
                                    expr = expr.replace(/\bAND\b/g, context.__(' AND ')).replace(/\bOR\b/g, context.__(' OR ')).replace(/\bNOT\b/g, context.__(' NOT '));
                                    return expr;
                                }
                                catch (e) {
                                    return e;
                                }
                            };

                            const finalValidationResults = [];
                            additionalTypes.forEach(function (t) {
                                let res, title;
                                //filter by rule type
                                const filtered = rulesData.filter(function (x) {
                                    return x['additionalType'] === t;
                                });
                                if (filtered.length > 0) {
                                    //apply default expression
                                    const expr = filtered.find(function (x) {
                                        return !_.isEmpty(x['ruleExpression']);
                                    });
                                    if (expr) {
                                        res = fnValidateExpression(expr);
                                        title = fnTitleExpression(expr);
                                    }
                                    else {
                                        //get expression (for this rule type)
                                        const ruleExp1 = filtered.map(function (x) {
                                            return '[%' + x.id + ']';
                                        }).join(' AND ');
                                        res = fnValidateExpression({ ruleExpression: ruleExp1 });
                                        title = fnTitleExpression({ ruleExpression: ruleExp1 });
                                    }
                                    //build
                                    let finalResult = new ValidationResult(res, res === true ? 'SUCC' : 'FAIL', title);
                                    finalResult.type = t;
                                    const childValidationResults = [];
                                    filtered.forEach(function (x) {
                                        childValidationResults.push.apply(childValidationResults, validationResults.filter(function (y) {
                                            return x.id === y.id;
                                        }));
                                    });
                                    if (childValidationResults.length === 1) {
                                        if (finalResult.success === childValidationResults[0].success) {
                                            finalResult = childValidationResults[0];
                                        }
                                        finalResult.type = t;
                                    }
                                    else {
                                        finalResult.validationResults = childValidationResults;
                                    }
                                    finalValidationResults.push(finalResult);
                                }

                            });
                            callback(null, finalValidationResults)
                        });
                    });
                });
            });
        }
        catch (e) {
            callback(e);
        }
    }

    save(context, callback) {
        const self = this;
        let prevSpecialty='';
        let studentIdentifier;
        let studentName;
        try {
            self.validateState(function (err) {
                if (err) {
                    self.validationResult = new ValidationResult(false, err.code || 'EFAIL', context.__('Cannot change student specialty.'), err.message);
                    return callback();
                }
                self.studentOf(function (err, student) {
                    const students = self.context.model('Student');
                    context.unattended(function (cb) {
                        self.validate(function (err,validationResult) {
                            if (err) {
                                return cb(err);
                            }
                            if (typeof validationResult ==='undefined' || validationResult[0].success) {
                                self.context.model('Student').where("id").equal(student).select('id','studentIdentifier','name','specialty').first(function(err,res) {
                                    if (err) {return cb(err);}
                                    prevSpecialty=res["specialty"];
                                    studentIdentifier=res["studentIdentifier"];
                                    studentName=res["name"];
                                    students.save({id: student, specialtyId: self.specialty.specialty}, function (err) {
                                        if (err){return cb(err)}
                                        const eventTitle=util.format('Αλλαγή κατεύθυνσης φοιτητή [%s] %s από την κατεύθυνση <%s> στην κατεύθυνση <%s>'
                                            ,studentIdentifier,studentName,prevSpecialty,self.specialty.name);
                                        context.model('EventLog').save({title: eventTitle, eventType: 2, username: context.interactiveUser.name}, function (err) {
                                           return cb(err);
                                        });

                                    });
                                });
                            }
                            else {
                                return cb(validationResult[0]);
                            }
                        });

                    }, function (err) {
                        if (err) {
                            if (err instanceof ValidationResult) {
                                self.validationResult = err;
                            }
                            else {
                                TraceUtils.error(err);
                                self.validationResult = new ValidationResult(false, err.code || 'FAIL', self.context.__('Cannot change student specialty.'), err.message);
                            }
                        }
                        else {
                            self.validationResult = new ValidationResult(true, 'SUCC', self.context.__('Student specialty was changed successfully.'));
                        }
                        callback();
                    });
                });
            });
        }
        catch (er) {
            callback(er);
        }
    }

    validateState(done) {
        const self = this, context = self.context;
        //get programSpecialty object
        self.programSpecialty(function (err, specialty) {
            if (err) {
                return done(err);
            }
            self.studentOf(function (err, result) {
                if (err) {
                    return done(err);
                }
                if (_.isNil(result)) {
                    return done(new Error('Student is null or undefined'));
                }
                /**
                 *
                 * @type {Student|*}
                 */
                const student = context.model('Student').convert(result);
                if (_.isNil(self.student))
                    self.student=result;

                //check student status
                student.canSelectSpecialty(function (err) {
                    if (err) {
                        return done(err);
                    }
                    //check if new specialty exists in available specialties
                    student.availableProgramSpecialties(function (err, specialties) {
                        if (err) {
                            return done(err);
                        }
                        const currentSpecialty = specialties.filter(function (x) {
                            return (x.id === specialty.id);
                        });
                        if (_.isNil(currentSpecialty)) {
                            return done(new Error(context.__('Program specialty cannot be found.')));
                        }
                        done();
                    });
                });
            });
        });
    }

    programSpecialty(callback) {
        const self = this;
        let specialtyId = self.specialty;
        const context = self.context;
        if (_.isObject(self.specialty)) {
            specialtyId = self.specialty.id;
        }
        context.model("StudyProgramSpecialty").where("id").equal(specialtyId).flatten().silent().first(function (err, specialty) {
            if (err) {
                return callback(err);
            }
            if (_.isNil(specialty)) {
                return callback(new Error(context.__('Program specialty cannot be found.')));
            }
            self.specialty=specialty;
            callback(null, specialty);
        });
    }
}

StudentSpecialty.prototype.studentOf = StudentMapper.prototype.studentOf;

module.exports = StudentSpecialty;