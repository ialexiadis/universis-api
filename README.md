# UniverSIS
[UniverSIS](https://universis.gr) is a coordinated effort by Greek academic institutions to build a Student Information System as an open source platform. The target is to serve our common needs to support academic and administrative processes.

#### Installation prerequisites

**1. Install node.js** 

Node.js version > 6.14.3 is required. Visit [Node.js](https://nodejs.org/en/) and follow the installation instructions provided for your operating system.

**2. Install Ngnix or apache web server (Optional)**

A frontend web server may be installed and used as a proxy server for any node.js application which is a part of universis project.
This web server may be the [nginx web server](https://nginx.org/en/), the [Apache HTTP server](https://httpd.apache.org/) or any other server application which may operate as proxy server.

**_Nginx web server installation_**

[How to install Nginx on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-16-04)

[How to install Nginx on CentOS 7](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-centos-7)

[How to install Nginx on Windows](https://vexxhost.com/resources/tutorials/nginx-windows-how-to-install/)


**_Apache web server 2.4 installation_**

[How to install the Apache Web Server on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-ubuntu-16-04)

[How to install Apache Web Server on CentOS 7](https://www.linode.com/docs/web-servers/apache/install-and-configure-apache-on-centos-7/)

[How to install Apache Web Server on Windows](https://httpd.apache.org/docs/2.4/platform/windows.html)

**3. Install PM2 Process Manager (Optional)**

[PM2 Advanced Process Manager for node.js](http://pm2.keymetrics.io/) may be installed by following the installation instructions 
provided in [PM2 quick start article](http://pm2.keymetrics.io/docs/usage/quick-start/)

    npm install pm2@latest -g 
    
[How to install pm2 as service on Linux Distributions](http://pm2.keymetrics.io/docs/usage/startup/)

[How to install pm2 as service on Windows](https://www.npmjs.com/package/pm2-windows-service)
     
#### Installation

**1. Checkout source code**

Checkout source code by executing the following command

    git clone https://gitlab.com/universis/universis-api.git

Go to project directory:

    cd universis-api
    
**2. Install dependencies**

Install application development dependencies by executing the following command:

    npm i
    
**3. Configure application**

Read "Configuration" section below

**4. Build from source code**

Build source code by executing the following command:

    npm run build
    
**5. Register PM2 Process ()**

Create configuration file for pm2.config.json

    {
        "name"        : "univesis_api",
        "script"      : "./bin/www",
        "watch"       : true,
        "env": {
          "IP": "0.0.0.0"
        }
    }

and set the binding IP address in environment variable "IP".

Register PM2 process by executing:

    pm2 start pm2.config.json
    
Navigate to http://localhost:5001

### Docker container installation

Navigate to application root directory and build docker image:

    # build docker image
    docker build . -t universis-api:latest
    
Create docker container

    docker run -d \
      --name=api \
      --mount source=api,destination=/usr/src/app/server/config \
      universis-api:latest

Edit src/config/app.production.json in docker mount volume /var/lib/docker/volumes/api/_data/
and set client_id and client_secret of OAuth2 Server Application

    "settings": {
    ...
        "auth": {
            ...
            "client_id":".....",
            "client_secret":"...."
        },
    ...
    }

Restart container

    docker restart api

Note: Read "Configuration" section below for further information about application configuration

    
#### Configuration

Application configuration base settings are defined in server/config/app.json file.
Copy server/config/app.json as app.production.json

    cp server/config/app.json server/config/app.production.json
    
or

    copy server\config\app.json server\config\app.production.json

on Windows.

Open production configuration app.production.json:

    {
        "services": [
            { "serviceType": "./services/oauth2-client-service#OAuth2ClientService" },
            { "serviceType": "./services/swagger-service#SwaggerService" }
        ],
        "settings": {
            "i18n": {
                "locales": [ "en","el" ],
                "defaultLocale": "en"
            },
            "auth": {
                "unattendedExecutionAccount":"b86YUWP5SC7N==",
                "server_uri":"http://auth.example.com",
                "client_id":"1234567890123",
                "client_secret":"Bh8BKWsdH5TdCNShBpq7DoMSfeJHmeDBupRa",
                "callback_uri":"http://localhost:5001/auth/callback"
            },
            "crypto": {
                "algorithm": "aes256",
                "key": "3767364478355554435438466f594e6f7855666b764a584235754c705765786675764d565362754d54726634366771644551646b32595451517555647a347146"
            }
        },
        "adapterTypes": [
            { "name":"MSSQL Data Adapter", "invariantName": "mssql", "type":"@themost/mssql" }
        ],
        "adapters": [
            {
                "name": "development",
                "default": true,
                "invariantName": "mssql",
                "options": {
                    "server": "localhost",
                    "user": "node",
                    "password": "pass",
                    "database": "universis",
                    "requestTimeout":30000
                }
            }
        ]
    }


**settings.i18n**

This section describes the localization settings for this application.

_settings.localization.locales_

Sets an array of the available locales like ["en","el"]

_settings.localization.defaultLocale_

Sets the default culture of this application

**settings.crypto**

This section describes the cryptographic algorithm and the cryptographic key which are going to used by
 @themost/express DefaultEncyptionStrategy service. 
 The cryptographic key is a random hex string and it is important to generate a new one for every application instance.
 The key length must be 32, 48, 64 or 128.
 
    "crypto": {
              "algorithm": "aes256",
              "key": "3767364478355554435438466f594e6f7855666b764a584235754c705765786675764d565362754d54726634366771644551646b32595451517555647a347146"
            }
 
 Note: You can use [@themost/cli](https://github.com/kbarbounakis/most-web-cli) to generate a new key:
 
    npm run themost -- random hex --length 48 
    
**settings.auth**

This section defines general authentication settings and other settings for connecting with local MOST Web Framework OAuth2 Server.

    "auth": {
                "unattendedExecutionAccount":"b86YUWP5SC7N==",
                "server_uri":"http://auth.example.com",
                "client_id":"1234567890123",
                "client_secret":"Bh8BKWsdH5TdCNShBpq7DoMSfeJHmeDBupRa",
                "callback_uri":"http://localhost:5001/auth/callback"
            }

_settings.auth.unattendedExecutionAccount_

Sets the unattended execution account for internal application processes. It is better to avoid using a valid application account. Define a random string

Note: You can use [@themost/cli](https://github.com/kbarbounakis/most-web-cli) to generate a random string:
 
    npm run themost -- random string --length 16 

_settings.auth.server_uri_

Defines the OAuth2 Server URL

_settings.auth.client_id_

Defines the client id  of an OAuth2 Server authorized client 

_settings.auth.client_secret_

Defines the client secret of an OAuth2 Server authorized client 

_settings.auth.callback_uri_

Defines the callback URL that is going to be used by OAuth2 Server for redirecting user back to this application 

**adapterTypes**

Defines an array of available data adapter types for this application. These adapter types are going to be used for connecting with application data storage.
The default adapter type for Universis Application Server is the [@themost/mssql](https://github.com/themost-framework/themost-adapters/tree/master/modules/%40themost/mssql) adapter for connecting with MSSQL database server. 

    "adapterTypes": [
            ...
            { "name":"MSSQL Data Adapter", "invariantName": "mssql", "type":"@themost/mssql" },
            { "name":"Pool Data Adapter", "invariantName": "pool", "type":"@themost/pool" }
            ...
          ]

Note: [@themost/pool](https://github.com/themost-framework/themost-adapters/tree/master/modules/%40themost/pool) Adapter may be used for controlling connection pooling.

**adapters**

This section contains information about connecting with data storages used by this application.

Important Note: You can set the default adapter by setting adapter.default to true 

    "adapters": [
            {
                "name": "development",
                "default": true,
                "invariantName": "mssql",
                "options": {
                    "server": "localhost",
                    "user": "node",
                    "password": "pass",
                    "database": "universis",
                    "requestTimeout":30000
                }
            }
          ]

If you are intended to use generic pool adapter add a new item in adapters' collection, define the name of the base adapter and set the pool size:

      { "name":"development_with_pool", "invariantName":"pool", "default":false,
          "options": {
              "adapter":"development",
              "size": 25
          }
      }
      
#### Nginx Configuration

If you are using Nginx as web server create a configuration file for Universis Application Server (universis-api.conf) and place it in nginx configuration directory:

    server {
        listen 80;
        server_name universis.example.com;
        # redirects both www and non-www to ssl port
        return 301 https://universis.example.com/;
    }
    
    server {
        listen              443 ssl;
        server_name         universis.example.com;
        ssl_certificate     /etc/nginx/ssl/server.bundled.crt;
        ssl_certificate_key /etc/nginx/ssl/server.key;
            location / {
                    proxy_pass http://127.0.0.1:5001;
                    proxy_http_version 1.1;
                    proxy_set_header Upgrade $http_upgrade;
                    proxy_set_header Connection "upgrade";
            }
    }
    
Important Note: The default port of Universis Application Server is 5001. If you want to change this port 
you need to define PORT environment variable in application startup script (See PM2 Process Manager documentation on how to define environment variables)


#### Apache Configuration

If you are using Apache as web server enable apache proxy modules

[How To Use Apache HTTP Server As Reverse-Proxy Using mod_proxy Extension](https://www.digitalocean.com/community/tutorials/how-to-use-apache-http-server-as-reverse-proxy-using-mod_proxy-extension)


Create a configuration file for Universis Application Server (universis-api.conf) and place it in apache configuration directory:

    <VirtualHost *:80>
    
      ServerName universis.example.com
      ServerAlias universis.example.com
      RewriteEngine On 
      RewriteCond %{HTTPS} !=on 
      RewriteRule ^/?(.*) https://%{SERVER_NAME}/$1 [R,L]
      
    </VirtualHost>
    
    <VirtualHost *:443>
    
      SSLEngine on
      ServerName universis.example.com
      ServerAlias universis.example.com
      SSLCertificateFile "/etc/apache2/conf/ssl/server.crt"
      SSLCertificateKeyFile "/etc/apache2/conf/ssl/server.key"
      SSLCACertificateFile "/etc/apache2/conf/ssl/server-ca-bundled.crt"
    
      ProxyPass / http://127.0.0.1:5001/
      ProxyPassReverse /  http://127.0.0.1:5001/
      
    </VirtualHost>

## Documentation
The documentation is under development. Two core parts of the documentation are the API methods the schema models
https://www.universis.io/api-docs/
https://api.universis.io/api-docs/